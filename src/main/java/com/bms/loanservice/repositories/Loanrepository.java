package com.bms.loanservice.repositories;

import org.springframework.data.repository.CrudRepository;

import com.bms.loanservice.entity.Loanentity;

public interface Loanrepository extends CrudRepository<Loanentity, Integer> {

}
