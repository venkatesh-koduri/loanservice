package com.bms.loanservice.servie;

import com.bms.loanservice.customizedexceptions.JsonExceptions;
import net.sf.json.JSONObject;

public interface Loanservice {

	Boolean applyLoan(JSONObject jsondata) throws JsonExceptions;

}
