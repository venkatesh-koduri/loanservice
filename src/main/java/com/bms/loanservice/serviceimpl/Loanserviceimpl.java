package com.bms.loanservice.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bms.loanservice.customizedexceptions.JsonExceptions;
import com.bms.loanservice.entity.Loanentity;
import com.bms.loanservice.model.Loanmodel;
import com.bms.loanservice.repositories.Loanrepository;
import com.bms.loanservice.servie.Loanservice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.json.JSONObject;

@Service
public class Loanserviceimpl implements Loanservice {
	
	@Autowired
	private Loanrepository repo;

	@Override
	public Boolean applyLoan(JSONObject jsondata) throws JsonExceptions {
		boolean iscreated = true;
		try {
			Loanmodel model = new ObjectMapper().readValue(jsondata.toString(), Loanmodel.class);
			Loanentity entity = prepareloanentity(model);
			repo.save(entity);
			
		} catch (JsonProcessingException e) {
			iscreated = false;
			throw new JsonExceptions("Json Processing Exception");
		}
		return iscreated;
	}

	private Loanentity prepareloanentity(Loanmodel model) {
		Loanentity entity = new Loanentity();
		entity.setDol(model.getDol());
		entity.setLoanamt(model.getLoanamt());
		entity.setLoantype(model.getLoantype());
		entity.setRoi(model.getRoi());
		entity.setCustomerid(model.getCustomerid());
		return entity;
	}

}
