package com.bms.loanservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bms.loanservice.customizedexceptions.JsonExceptions;
import com.bms.loanservice.servie.Loanservice;
import net.sf.json.JSONObject;

@RestController
@RequestMapping(value = "api/v1/loan")
public class Loancontroller {
	
	@Autowired
  	private Loanservice service;
	
	@PostMapping(value= "/applyloan", consumes = "application/json")
	public Boolean applyLoan(@RequestBody JSONObject jsondata) throws JsonExceptions{
		return service.applyLoan(jsondata);
	}

}
