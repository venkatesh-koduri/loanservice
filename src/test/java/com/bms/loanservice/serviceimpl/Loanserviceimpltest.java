package com.bms.loanservice.serviceimpl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.bms.loanservice.customizedexceptions.JsonExceptions;
import com.bms.loanservice.entity.Loanentity;
import com.bms.loanservice.repositories.Loanrepository;

import net.sf.json.JSONObject;

@SpringBootTest
class Loanserviceimpltest {
	
	@Mock
	private Loanrepository repo;
	
	@InjectMocks
	private Loanserviceimpl serviceimpl;
	
	
	@BeforeEach
	private void init() {
	   MockitoAnnotations.openMocks(this);

	}
	

	@Test()
	public void applyLoanTest() throws JsonExceptions {
		
		when(repo.save(new Loanentity())).thenReturn(new Loanentity());
		JSONObject jo = new JSONObject();
		jo.put("loantype", "CAR LOAN");
		jo.put("loanamt", 700000);
		jo.put("roi", 6);
		jo.put("dol", 36);
		jo.put("customerid", 1);
		assertEquals(true, serviceimpl.applyLoan(jo));
	}
	
	
	@Test()
	public void applyLoanTestException() {
		
		when(repo.save(new Loanentity())).thenReturn(new Loanentity());
		JSONObject jo = new JSONObject();
		jo.put("loantype", "CAR LOAN");
		jo.put("loanamt", 700000);
		jo.put("roi", "dsfddfds");
		jo.put("dol", 36);
		jo.put("customerid", 1);
		Exception exp = assertThrows(JsonExceptions.class, () -> serviceimpl.applyLoan(jo));
		assertEquals("Json Processing Exception", exp.getMessage());
	}


	
}
